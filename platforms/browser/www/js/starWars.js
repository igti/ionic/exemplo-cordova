/**
 * 'Estado' da aplicação através
 * de um vetor de strings (personagens)
 */
var personagens = [
  'Luke Skywalker',
  'Leia Organa',
  'Han Solo',
  'Chewbacca',
  'Darth Vader'
];

/**
 * Referências do botão e input
 */
var botao = null;
var input = null;

/**
 * Função de inicialização, que será
 * disparada em onDeviceReady (index.js)
 */
function inicializarStarWars() {
  botao = document.querySelector('#btAddPersonagem');
  input = document.querySelector('#inputNovoPersonagem');

  botao.addEventListener('click', botaoClicado);
  input.addEventListener('keyup', validarDigitacao);

  carregarPersonagens();
  limparInputComFoco();
}

/**
 * Evento acionado pelo botão
 */
function botaoClicado() {
  var personagem = document.querySelector('#inputNovoPersonagem').value;
  validarPersonagem(personagem);
}

/**
 * Validação das teclas digitadas pelo usuário
 * no input
 * @param {Event} event Evento de keyup do input
 */
function validarDigitacao(event) {
  var keyCode = event.keyCode;

  /**
   * Se o usuário digitou ENTER (13),
   * é feita a inclusão do personagem
   */
  if (keyCode === 13) {
    validarPersonagem(event.target.value);
  }
}

/**
 * Valida o personagem e insere se estiver
 * tudo ok
 * @param {string} personagem Personagem a ser validado
 */
function validarPersonagem(personagem) {
  /**
   * Persistindo estado da aplicação
   */
  personagens.push(personagem);
  inserirPersonagem(personagem);
  limparInputComFoco();
}

/**
 * Limpando input e marcando foco
 */
function limparInputComFoco() {
  input.value = '';
  input.focus();
}

/**
 * Percorre o vetor de personagens e
 * insere cada um deles na tabela através
 * da função 'novoPersonagem'
 */
function carregarPersonagens() {
  for (var i = 0; i < personagens.length; i++) {
    inserirPersonagem(personagens[i]);
  }
}

/**
 * Insere um novo elemento na tabela através
 * de appendChild
 * @param {string} personagem Novo personagem
 */
function inserirPersonagem(personagem) {
  /**
   * Atualizando HTML
   */
  var tr = document.createElement('tr');
  var td = document.createElement('td');
  td.textContent = personagem;
  tr.appendChild(td);

  var tabela = document.querySelector('#tabela');
  tabela.appendChild(tr);
}
